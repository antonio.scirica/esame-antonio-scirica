<?php get_header(); ?>

<!-- s-content
================================================== -->
<section class="s-content">

    <div class="row entries-wrap wide">
        <div class="entries">


          <div class="col-seven md-six tab-full popular">
          					<?php
          					// Search results.
          					if ( is_search() ) {

          					global $wp_query;

          					$page_heading = sprintf(
          						'%1$s %2$s',
          						'<h3>' . __( 'Search results for', 'esame' ),
          						'&ldquo;' . get_search_query() . '&rdquo;</h3>'
          					);

          					if ( $wp_query->found_posts ) {

          						$page_subheading = sprintf(
          							_n(
          								'We found %s post for your search',
          								'We found %s posts for your search',
          								$wp_query->found_posts,
          								'tema-ied'
          							),
          							number_format_i18n( $wp_query->found_posts )

          						);
          					} else {
          						$page_subheading = __( 'Sorry, but there are no results for your search query.
                      Try searching again using the form on the right or go back to the <a href="' . esc_url( home_url( '/' ) ) . '">Homepage</a>', 'esame' );
          					}

          					if ( $page_heading && $page_subheading ) {
          						echo $page_heading . '<br>' . $page_subheading;
          					}
          					?>
                  </div>

          				<?php

          				}

 if(have_posts()); ?>
<?php while(have_posts()) : the_post();
  if ( $post->ID !== $featured_id ) : ?>
            <article class="col-block">

                <div class="item-entry" data-aos="zoom-in">
                    <div class="item-entry__thumb">
                      <?php the_post_thumbnail('full'); ?>
                    </div>

                    <div class="item-entry__text">
                        <div class="item-entry__cat">
                            <a href="category.html"><?php the_category(); ?></a>
                        </div>

                        <a href="<?php the_permalink(); ?>"><h1 class="item-entry__title"><?php the_title(); ?></h1></a>


                        <div class="item-entry__date">
                            <a href="single.php"><?php the_time( 'F j, Y'); ?></a>
                        </div>
                    </div>
                </div> <!-- item-entry -->

            </article> <!-- end article -->
<?php endif; endwhile; ?>






        </div> <!-- end entries -->
    </div> <!-- end entries-wrap -->



</section> <!-- end s-content -->



<?php get_footer(); ?>
