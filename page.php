<?php get_header(); ?>
    <!-- s-content
    ================================================== -->
    <section class="s-content s-content--top-padding s-content--narrow">

        <div class="row narrow">
            <div class="col-full s-content__header">
                <h1 class="display-1 display-1--with-line-sep"><?php the_title(); ?></h1>
            </div>
        </div>

        <div class="row">
            <div class="col-full s-content__main">
                <p>
                  <?php the_post_thumbnail('full'); ?>
                </p>

                <h2><?php the_title(); ?></h2>
                <?php while (have_posts()) : the_post(); ?>
                  <p><?php the_content(); ?></p>
                  <?php endwhile; ?></p>
            </div> <!-- s-content__main -->
        </div> <!-- end row -->

    </section> <!-- end s-content -->
  <?php get_footer(); ?>
