<?php get_header(); ?>

<!-- featured
================================================== -->
<main class= 'contenitore'>
<section class="s-featured">
    <div class="row">
        <div class="col-full">

            <div class="featured" data-aos="zoom-in">

                <div class="featured__slide">
                    <div class="entry">

                        <div class="entry__background" style="background-image:url('<?php bloginfo('template_directory'); ?>/images/thumbs/featured/Carte.jpg');"></div>

                        <?php if ( is_front_page() )
                          {
                            include TEMPLATEPATH . '/featured-post.php';
                          }
                        ?>

                    </div> <!-- end entry -->
                </div> <!-- end featured__slide -->

        </div> <!-- end col-full -->
    </div>
</section> <!-- end s-featured -->


<!-- s-content
================================================== -->
<section class="s-content">

    <div class="row entries-wrap wide">
        <div class="entries">
<?php if(have_posts()): ?>
<?php while(have_posts()) : the_post();
  if ( $post->ID !== $featured_id ) : ?>
  <?php  get_template_part('content/content', 'excerpt') ?>

<?php endif; endwhile; ?>
<?php else: get_template_part('content/content', 'none');


endif; ?>









        </div> <!-- end entries -->
    </div> <!-- end entries-wrap -->

    <div class="row pagination-wrap">
        <div class="col-full">
          <?php the_posts_pagination(
                          array(
                            'prev_text' => sprintf( '<span class="previous-posts">%s</span>',
                              __( 'Previous', 'esame' )
                            ),

                            'next_text' => sprintf( '<span class="next-posts">%s</span>',
                              __( 'Next', 'esame' )
                            ),
                          )
                        ); ?>
        </div>
    </div>


</section> <!-- end s-content -->


<!-- s-extra
================================================== -->
<section class="s-extra">

    <div class="row">

        <div class="col-seven md-six tab-full popular">
            <h3><?php _e('Popular Posts', 'esame'); ?></h3>


            <div class="block-1-2 block-m-full popular__posts">
              <?php while(have_posts()) : the_post();
                if ( $post->ID !== $featured_id ) : ?>

                <article class="col-block popular__post">
                  <div class="item-entry__thumb">
                    <?php the_post_thumbnail('thumbnail'); ?>
                  </div>
                    <a href="<?php the_permalink(); ?>"<h5><?php the_title(); ?></h5></a>
                    <p><?php the_excerpt(); ?><a href="<?php the_permalink(); ?>"><?php _e('ReadMore', 'esame'); ?> &raquo;</a></p>
                    <section class="popular__meta">
                        <span class="popular__author"><span><?php _e('By', 'esame'); ?></span> <a href="php"><?php the_author(); ?></a></span>

                        <span class="popular__date"><span><?php _e('On', 'esame'); ?></span> <time datetime="2018-06-14"><?php the_time( 'F j, Y'); ?></time></span>
                    </section>
                </article>
                <?php endif; endwhile; ?>


            </div> <!-- end popular_posts -->
        </div> <!-- end popular -->

        <div class="col-four md-six tab-full end">
            <div class="row">
                <div class="col-six md-six mob-full categories">
                    <h3><?php _e('Categories', 'esame'); ?></h3>



                    <ul class="linklist">
                        <li><a href="#0"><?php the_category(); ?></a></li>
                    </ul>

                </div> <!-- end categories -->


            </div>
        </div>
    </div> <!-- end row -->

</section> <!-- end s-extra -->
</main>
<?php get_footer(); ?>
