<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>

    <!--- basic page needs
    ================================================== -->
    <meta charset="utf-8">
    <title><?php bloginfo('title'); ?></title>
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- mobile specific metas
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">


    <!-- favicons
    ================================================== -->
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <?php wp_head(); ?>
</head>

<body id="top">

    <!-- preloader
    ================================================== -->
    <div id="preloader">
        <div id="loader" class="dots-fade">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>


    <!-- header
    ================================================== -->
    <header class="s-header header">

        <div class="header__logo">
            <a class="logo" href="index.php">
                <img src="<?php bloginfo('template_directory'); ?>/images/logo.svg" alt="Homepage">
            </a>
        </div> <!-- end header__logo -->


        <a class="header__search-trigger" href="#0"></a>
        <div class="header__search">

            <form role="search" method="get" class="header__search-form" action="#">
                <label>
                    <span class="hide-content"><?php _e('Search for:', 'esame'); ?></span>
                    <input type="search" class="search-field" placeholder="Type Keywords" value="" name="s" title="Search for:" autocomplete="off">


                </label>

                <input type="submit" class="search-submit" value="Search">
            </form>


            <a href="#0" title="Close Search" class="header__overlay-close"><?php _e('Close', 'esame'); ?></a>

        </div>  <!-- end header__search -->

        <a class="header__toggle-menu" href="#0" title="Menu"><span><?php _e('Menu', 'esame'); ?></span></a>
        <nav class="header__nav-wrap">

            <h2 class="header__nav-heading h6"><?php _e('Navigate to', 'esame'); ?></h2>

            <?php get_template_part( 'nav-menu' );?>
            

            <a href="#0" title="Close Menu" class="header__overlay-close close-mobile-menu">Close</a>
        </nav> <!-- end header__nav-wrap -->


    </header> <!-- s-header -->
