<?php get_header(); ?>
<!-- s-content

    ================================================== -->
    <section class="s-content s-content--top-padding s-content--narrow">

        <article class="row entry format-standard">



              <div class="entry__header col-full">
                <?php while(have_posts()) : the_post();
                  if ( $post->ID !== $featured_id ) : ?>
                  <h1 class="entry__header-title display-1">
                      <?php the_title(); ?>
                  </h1>
                  <ul class="entry__header-meta">
                      <li class="date"><?php the_time( 'F j, Y'); ?></li>

                      <li class="byline">
                          <?php _e('By', 'esame'); ?>
                          <?php the_author(); ?>
                      </li>
                  </ul>

              </div>

              <div class="col-full entry__main">

                  <p><?php the_content(); ?>

                    <?php echo get_post_meta($post->ID, 'name', true); ?>
                    <?php wp_link_pages( array(
                  							'before' => '<div>' . __( 'Pages:', 'esame' ),
                  							'after'  => '</div>',
                  						)
                  					);?></p>


                    <div class="entry__tags">
                        <h5><?php _e('Tags: ', 'esame'); ?> </h5>
                        <span class="entry__tax-list entry__tax-list--pill">
                            <?php the_tags(); ?>
                        </span>
                    </div> <!-- end entry__tags -->
                </div> <!-- end s-content__taxonomies -->
                <?php endif; endwhile; ?>
            </div> <!-- s-entry__main -->

        </article> <!-- end entry/article -->

<?php while(have_posts()) : the_post();?>
        <div class="s-content__entry-nav">
            <div class="row s-content__nav">
                <div class="col-six s-content__prev">
                    <a href="<?php the_permalink(); ?>" rel="prev">
                      <span <?php previous_post(); ?></span>

                    </a>
                </div>
                <div class="col-six s-content__next">
                    <a href="#0" rel="next">
                      <a href="<?php the_permalink(); ?>" rel="prev">
                        <span <?php next_post(); ?></span>
                    </a>
                </div>
            </div>
        </div> <!-- end s-content__pagenav -->
      <?php endwhile; ?>




      <!-- s-extra
      ================================================== -->
      <section class="s-extra">

          <div class="row">

              <div class="col-seven md-six tab-full popular">
                  <h3><?php _e('Popular Posts', 'esame'); ?></h3>


                  <div class="block-1-2 block-m-full popular__posts">
                    <?php while(have_posts()) : the_post();
                      if ( $post->ID !== $featured_id ) : ?>

                      <article class="col-block popular__post">
                        <div class="item-entry__thumb">
                          <?php the_post_thumbnail('thumbnail'); ?>
                        </div>
                          <h5><?php the_title(); ?></h5>
                          <section class="popular__meta">
                              <span class="popular__author"><span><?php _e('By', 'esame'); ?></span> <a href="#0"><?php the_author(); ?></a></span>
                              <span class="popular__date"><span><?php _e('On', 'esame'); ?></span> <time datetime="2018-06-14"><?php the_time( 'F j, Y'); ?></time></span>
                          </section>
                      </article>
                      <?php endif; endwhile; ?>


                  </div> <!-- end popular_posts -->
              </div> <!-- end popular -->

              <div class="col-four md-six tab-full end">
                  <div class="row">
                      <div class="col-six md-six mob-full categories">
                          <h3><?php _e('Categories', 'esame'); ?></h3>



                          <ul class="linklist">
                              <li><a href="#0"><?php the_category(); ?></a></li>
                          </ul>

                      </div> <!-- end categories -->

                      

                  </div>
              </div>
          </div> <!-- end row -->

      </section> <!-- end s-extra -->

<?php get_footer(); ?>
