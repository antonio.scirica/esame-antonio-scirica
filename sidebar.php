<?php
/**
 * Sidebar.
 */
?>
<section class="s-extra">

    <div class="row">

      <div class="col-12">
          <div class="row">
          <aside class="main-sidebar">
              <?php
          	if ( function_exists( 'dynamic_sidebar' ) ) {
          		dynamic_sidebar( 'right-sidebar' );
          	}

          	?>
          </aside>
        </div>
      </div>
    </div>
  </section>
