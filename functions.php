<?php
require_once dirname( __FILE__ ) . '/shortcodes.php';

function esame_enqueue_styles_and_scripts()
{
    wp_enqueue_style('stylesheet', get_stylesheet_uri());
    wp_enqueue_style('bootstrap', get_template_directory_uri(). '/css/base.css');
    wp_enqueue_style('bootstrap1', get_template_directory_uri(). '/css/vendor.css');
    wp_enqueue_style('bootstrap2', get_template_directory_uri(). '/css/main.css');

    wp_enqueue_script('script1', get_template_directory_uri(). '/js/modernizr.js', array('jquery'),true, mt_rand(0, 4));
    wp_enqueue_script('script2', get_template_directory_uri(). '/js/jquery-3.2.1.min.js',true, mt_rand(0, 4));
    wp_enqueue_script('script3', get_template_directory_uri(). '/js/plugins.js', array('jquery'),true, mt_rand(0, 4));
    wp_enqueue_script('script4', get_template_directory_uri(). '/js/main.js', array('jquery'),true, mt_rand(0, 4));
}

add_action( 'wp_enqueue_scripts', 'esame_enqueue_styles_and_scripts' );

function esame_init() {

  register_taxonomy(
    		'portfolio_categories',
    		'portfolio',
    		array(
    			'label'             => __( 'Portfolio Categories', 'esame' ),
    			'public'            => true,
    			'show_admin_column' => true,
    			'rewrite'           => array( 'slug' => 'portfolio-categories' ),
    			'hierarchical'      => true,
    		)
    	);

  // Register dynamic sidebar.
  register_sidebar( array(
    'name' => 'Right Sidebar',
    'id'   => 'right-sidebar',
  ) );
}

function esame_register_portfolio_post_type() {

  	//Register `Portfolio` Custom Post Type.
  	register_post_type( 'portfolio', array(
  		'labels'      => array(
  			'name'          => __( 'Portfolio', 'esame' ),
  			'singular_name' => __( 'Portfolio Item', 'esame' ),
  		),
  		'public'      => true,
  		'menu_icon'   => 'dashicons-format-image',
  		'supports'    => array(
  			'title',
  			'editor',
  			'thumbnail',
  			'excerpt',
  			'custom-fields',
  			'author',
  			'revisions',
  		),
  		'taxonomies'  => array(
  			'categories',
  			'post_tag',
  			'category'
  		),
  		'has_archive' => true,
  		'rewrite'     => array( 'slug' => 'portfolio' ),
  	) );
  }

  function esame_flush_rewrite_rules() {
  	esame_register_portfolio_post_type();
  	flush_rewrite_rules();
  }

  add_action( 'init', 'esame_init' );
  add_action( 'init', 'esame_register_portfolio_post_type' );
  add_action( 'after_switch_theme', 'esame_flush_rewrite_rules' );




function esame_after_setup_theme(){

    load_theme_textdomain('esame');

    set_post_thumbnail_size( 160, 160, true );
    add_image_size( 'custom-image-size', 700, 420, true );
    add_theme_support( 'menus' );
    add_theme_support( 'title-tag' );
    the_post_thumbnail( 'thumbnail' );
    add_theme_support( 'post-thumbnails' );
    register_nav_menus( array(

  		'main-menu'   => 'Main Menu',
  		'footer-menu' => 'Footer Menu',
  		'social-link' => 'Social Menu',

  	) );
  }
  add_action( 'after_setup_theme', 'esame_after_setup_theme' );






?>
