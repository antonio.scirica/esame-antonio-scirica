<article class="col-block">

    <div class="item-entry" data-aos="zoom-in">
        <div class="item-entry__thumb">
          <?php the_post_thumbnail('full'); ?>
        </div>

        <div class="item-entry__text">
            <div class="item-entry__cat">
              <?php if ( get_the_terms( get_the_ID(), 'portfolio_categories' ) ) {
                the_terms( get_the_ID(), 'portfolio_categories');
                }?>
            </div>

            <h1 class="item-entry__title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
            <?php
          // Display excerpt only in Archive.
  		if ( ! is_front_page() || ! is_home() ) {
  			the_excerpt();
  		}
  		?>

            <div class="item-entry__date">
                <a href="<?php the_permalink(); ?>"><?php the_time( 'F j, Y'); ?></a>
            </div>
        </div>
    </div> <!-- item-entry -->

</article> <!-- end article -->
