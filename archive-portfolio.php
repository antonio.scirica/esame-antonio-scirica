<?php get_header(); ?>



<section class="s-content">


    <div class="row entries-wrap wide">
      <h1 id="titoloarchive"><?php the_archive_title(); ?></h1>
      <hr>
    </hr>
        <div class="entries">
<?php if(have_posts()): ?>
<?php while(have_posts()) : the_post();?>

             <?php  get_template_part('content/content', 'portfolio') ?>
<?php endwhile; ?>
<?php endif; ?>

<?php the_posts_pagination(
                array(
                  'prev_text' => sprintf( '<span class="previous-posts">%s</span>',
                    __( 'Previous', 'esame' )
                  ),

                  'next_text' => sprintf( '<span class="next-posts">%s</span>',
                    __( 'Next', 'esame' )
                  ),
                )
              ); ?>
</section>



<!-- s-extra
================================================== -->
<section class="s-extra">

    <div class="row">



        <div class="col-12">
            <div class="row">
                <div class="col-six md-six mob-full categories">
                    <?php get_sidebar(); ?>
                </div> <!-- end categories -->


            </div>
        </div>
    </div> <!-- end row -->

</section> <!-- end s-extra -->

<?php get_footer(); ?>
