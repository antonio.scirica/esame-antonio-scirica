
<?php

$args = array(
	'posts_per_page' => 1, // how many posts.
);

$featured_post = new WP_Query( $args );

if ( $featured_post->have_posts() ) {
	while ( $featured_post->have_posts() ) {
		$featured_post->the_post();

		$featured_id = $post->ID;

		?>
    <div class="entry__content">
        <span class="entry__category"><?php the_category(); ?></span>

        <a href=" <?php the_permalink(); ?>"><h1><?php the_title(); ?></h1></a>

        <div class="entry__info">
            <a href="#0" class="entry__profile-pic">
                <img class="avatar" src="<?php bloginfo('template_directory'); ?>/images/avatars/user-05.jpg" alt="">
            </a>
            <ul class="entry__meta">
                <li><a href="#0"><?php the_author(); ?></a></li>
                <li><?php
				/**
				 * @see https://wordpress.org/support/article/formatting-date-and-time/
				 */
				?>
                <time><?php the_time( 'F j, Y'); ?></time></li>
            </ul>
        </div>
    </div>
		<?php
	}
}
