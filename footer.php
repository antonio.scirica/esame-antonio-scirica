<!-- s-footer
================================================== -->
<footer class="s-footer">

    <div class="s-footer__main">
        <div class="row">

            <div class="col-six tab-full s-footer__about">

                <h4><?php _e('About', 'esame'); ?> <?php the_author(); ?></h4>

                <p><?php _e('Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'esame'); ?></p>

            </div> <!-- end s-footer__about -->

            <div class="col-six tab-full s-footer__subscribe">

                <h4><?php _e('Our Newsletter', 'esame'); ?></h4>

                <p><?php _e('Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'esame'); ?></p>



            </div> <!-- end s-footer__subscribe -->

        </div>
    </div> <!-- end s-footer__main -->

    <div class="s-footer__bottom">
        <div class="row">

            <div class="col-six">
                <ul class="footer-social">
                    <li>
                        <a href="#0"><i class="fab fa-facebook"></i></a>
                    </li>
                    <li>
                        <a href="#0"><i class="fab fa-twitter"></i></a>
                    </li>
                    <li>
                        <a href="#0"><i class="fab fa-instagram"></i></a>
                    </li>
                    <li>
                        <a href="#0"><i class="fab fa-youtube"></i></a>
                    </li>
                    <li>
                        <a href="#0"><i class="fab fa-pinterest"></i></a>
                    </li>
                </ul>
            </div>

            <div class="col-six">
                <div class="s-footer__copyright">
                    <span><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
<?php _e('Copyright', 'esame'); ?> &copy;<script>document.write(new Date().getFullYear());</script> <?php _e('All rights reserved | This template is made with', 'esame'); ?><i class="fa fa-heart" aria-hidden="true"></i> <?php _e('By', 'esame'); ?> <a href="https://google.com" target="_blank"><?php the_author(); ?></a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
</span>
                </div>
            </div>

        </div>
    </div> <!-- end s-footer__bottom -->

    <div class="go-top">
        <a class="smoothscroll" title="Back to Top" href="#top"></a>
    </div>

</footer> <!-- end s-footer -->

<?php wp_footer(); ?>

</body>

</html>
